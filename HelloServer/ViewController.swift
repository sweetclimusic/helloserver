//
//  ViewController.swift
//  HelloServer
//
//  Created by Ashlee on 23/09/2019.
//  Copyright © 2019 Green Farm Games Ltd. All rights reserved.
//

import UIKit
import GCDWebServers

class ViewController: UIViewController {

    @IBOutlet weak var greetings: UILabel!
    var serverConnected = false
    var serverInstance: GCDWebServer!
    let welcomeMessage = "Welcome, click connect to start the server"
    var connectButton: UIButton!
    let defaultButtonLabel = "Launch Server"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        greetings.text = welcomeMessage
        // Reference the main view and find the button
        connectButton = (self.view.viewWithTag(1000) as! UIButton)
        toggleText()
    }
    
    @IBAction func startServer() {
        if(serverConnected){
            disconnectServer(webserver: serverInstance)
        }else {
            launchServer()
        }
    }
    func launchServer() {
        let webServer = GCDWebServer()
        buildRoutes(webserver: webServer)
        webServer.start(withPort: 3000, bonjourName: "GCD Web Server")
        if let serverUrl = webServer.serverURL  {
            greetings.text = "Visit \(serverUrl) in your web browser"
            serverConnected = webServer.isRunning
            toggleText()
        }
        serverInstance = webServer
    }
    
    func buildRoutes(webserver server: GCDWebServer) {
        server.addDefaultHandler(forMethod: "GET",
                                 request: GCDWebServerRequest.self,
                                 processBlock: {request in
            return GCDWebServerDataResponse(html:"<html><body><p>Hello World</p></body></html>")
        
        })
    }
    
    func disconnectServer(webserver server: GCDWebServer) {
        server.stop()
        if !server.isRunning {
            serverConnected = server.isRunning
            toggleText()
        }
    }
    private func toggleText() {
        if(!serverConnected){
            greetings.text = welcomeMessage
            connectButton.setTitle(defaultButtonLabel, for: .normal)
        }else{
            connectButton.setTitle("Disconnect", for: .normal)
        }
    }
}
