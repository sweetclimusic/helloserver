# Local iOS webserver

webserver is implmented using [GCDWEbServer](https://github.com/swisspol/GCDWebServer)
## Hello world

![](resources/demo.gif)

### Add Routes 
Go ahead and clone and remix the app by adding your own routes
```
server.addDefaultHandler(forMethod: "GET",
                                 request: GCDWebServerRequest.self,
                                 processBlock: {request in
            return GCDWebServerDataResponse(html:"<html><body><p>Hello World</p></body></html>")
        
        })
```

Moving forward I'd like to use this as a RESTApi for devices on my home network.
